package com.org.studentservice.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.org.studentservice.consumer.BookRestConsumer;
import com.org.studentservice.model.Book;

@RestController
@RequestMapping("/student")
public class StudentRestController {

      @Autowired
      private BookRestConsumer consumer;

      @GetMapping("/data")
      public String getStudentInfo() {
         System.out.println(consumer.getClass().getName());  //prints as a proxy class
         return "Data accessing from STUDENT-SERVICE ==> " +consumer.getBookData();
      }

      @GetMapping("/allBooks")
      public List<Book> getBooksInfo() {
         //return "Accessing from STUDENT-SERVICE ==> " + consumer.getAllBooks();
    	  return consumer.getAllBooks();
      }

      @GetMapping("/getOneBook/{id}")
      public Book getOneBookForStd(@PathVariable Integer id) {
         System.out.println("Accessing from STUDENT-SERVICE ==> " + consumer.getBookById(id)); 
         return consumer.getBookById(id);
      }

      @GetMapping("/entityData")
      public String printEntityData() {
         ResponseEntity<String> resp = consumer.getEntityData();
         return "Accessing from STUDENT-SERVICE ==> " + resp.getBody() +" , status is:" + resp.getStatusCode();
      }
}
